#!/usr/bin/pythonRoot

import cgi,cgitb
cgitb.enable() #for debugging

from gpiozero import Robot
from time import sleep

robby = Robot (left=(6,13), right=(26,19))

print "Content-type:text/html\n"


command_list = ['f','b','l','r'];
def backwards():
	print "Backwards";
	robby.backward()
	sleep(.2) # for < ~1 ft distance
	robby.stop()
        return;

def forwards():
        print "Forwards";
	robby.forward()
	sleep(.2) # for < ~1 ft distance
	robby.stop()
        return;

def left():
        print "Left";
	robby.left()
	sleep(.4) # for 90 deg turn
	robby.stop() 
       	return;

def right():
        print "Right";
	robby.right()
	sleep(.4) # for 90 deg turn
	robby.stop()
        return;

command_switcher = {
        'f': forwards,
        'b': backwards,
        'l': left,
        'r': right
}


#s = "l:3,f:7,b:2,r:4,f:11,s:1,b:4,f:3";
form = cgi.FieldStorage();
s = form.getvalue('searchbox');
#print s;

r = s.split(',');
#print r;


for item in r :
#       print item.split(':');
        t = item.split(':');
#       print t[0];
#       print t[1];
        if t[0] in command_list:
                if ( int(t[1]) > 0 and int(t[1]) < 10 ):
#                       print "Valid command, lets run it!\n";
			print "\n";
                        for a in range(int(t[1])):
                                func = command_switcher.get(t[0]);
                                func();
                else:
                        print "Out of valid range\n";
        else:
                print "Unknown command\n";

