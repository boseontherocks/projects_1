#!/usr/bin/pythonRoot

print "Content-type:text/html\n"
##print "<h1>This is the Home Page</h1>"

import cgi,cgitb
cgitb.enable() #for debugging

from gpiozero import Robot
from time import sleep
from gpiozero import DistanceSensor

sensor = DistanceSensor(17,27)
if ( sensor.distance*100 < 20):
	print "Distance is less than 20 cm, cannot proceed with command\n"
	raise SystemExit

robby = Robot (left=(6,13), right=(26,19))

robby.forward()
sleep(.5) # for < ~1 ft distance
robby.stop()

