#!/usr/bin/python

from gpiozero import DistanceSensor 
from time import sleep 

sensor = DistanceSensor(17, 27)

while True: 
    print('Distance to nearest object is', sensor.distance*100, 'cm') 
    sleep(1)
