from gpiozero import Robot
from time import sleep
    
robby = Robot (left=(6,13), right=(26,19))

while True:
        robby.forward()
        sleep(.9) # for < ~1 ft distance
        robby.stop()
        sleep(3)
        robby.right()
        sleep(.4) # for 90 deg turn
        robby.stop()
        sleep(3)